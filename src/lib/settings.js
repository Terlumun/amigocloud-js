const DEFAULT_SETTINGS = {
  baseUrl: 'https://app.amigocloud.com/',
  userId: undefined,
  accessToken: undefined
}

/**
 * Object which will provide with settings that will be used to connect to the amigocloud socket.io server
 */
class SettingsProvider {
  settings = DEFAULT_SETTINGS

  /**
   * Updates one or more settings necessary to connect to the backend
   * @function
   * @param {Object} settings - object with settings we want to override from the default configuration
   * @returns {void}
   * @example
   * settingsProviderInstance.set({
   *   baseUrl: 'http://beta4.amigocloud.com/', // custom server to where we want to connect
   *   userId: 1,
   *   accessToken: 'R:1234' // token generated at the /accounts/tokens/ url
   * })
   */
  set = settings => {
    this.settings = {
      ...this.settings,
      ...settings
    }
  }

  /**
   * @function
   * @returns {Object} with the settings either default or updated by the user
   */
  get = () => {
    return this.settings
  }
}

const settingsProviderInstance = new SettingsProvider()

export default settingsProviderInstance
