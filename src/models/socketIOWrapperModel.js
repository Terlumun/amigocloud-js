import { fromEvent } from 'rxjs'
import { filter, map, takeUntil } from 'rxjs/operators'

const formatSocketIOData = ({ data }) => {
  if (data) {
    const [eventName, eventData] = data

    return {
      eventName,
      eventData,
      fromJob: false
    }
  } else {
    return {}
  }
}

const generateEventsFilter = eventsList => {
  return ({ eventName }) => {
    return eventsList.indexOf(eventName) !== -1
  }
}

/**
 * @class
 * Model that will emit a new state any time a socket.io event is received
 */
class SocketIOModel {
  constructor (socketIOInstance) {
    this.socketIOInstance = socketIOInstance
    this.observer = fromEvent(this.socketIOInstance, '*').pipe(
      map(formatSocketIOData)
    )
  }

  /**
   * @function
   * @param {Array<String>} eventsList - Array of event names you want to listen
   * @param {Object} takeUntilObservable - observable that will indicate until when we want to listen to the events in eventsList
   * @returns {Object} observable - RxJs observable to which a client can subscribe to receive updates
   */
  listenOn = (eventsList, takeUntilObservable = null) => {
    if (takeUntilObservable) {
      return this.observer.pipe(
        filter(generateEventsFilter(eventsList)),
        takeUntil(takeUntilObservable)
      )
    } else {
      return this.observer.pipe(filter(generateEventsFilter(eventsList)))
    }
  }
}

export default SocketIOModel
