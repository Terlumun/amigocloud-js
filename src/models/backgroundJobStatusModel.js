import BaseModel from './baseModel'
import settings from '../lib/settings'

class BackgroundJobStatusModel extends BaseModel {
  constructor (jobId) {
    super(jobId)
    const { baseUrl } = settings.get()

    this.source = `${baseUrl}api/v1/me/jobs/${jobId}`
  }

  getModelId = () => {
    return `${this.constructor.name}${this.id ? `-${this.id}` : ''}`
  }

  transformData = ({ data }) => {
    return {
      job: data
    }
  }
}

export default BackgroundJobStatusModel
