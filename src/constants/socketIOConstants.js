const SOCKET_IO_SESSION_URL = 'api/v1/me/start_websocket_session'

const SOCKET_IO_SETTINGS = {
  server: 'amigosocket',
  path: '/v3_socket.io', // needs to have the "/" at the beginning
  transports: ['websocket'],
  reconnectionDelay: 8000,
  randomizationFactor: 1
}

const SOCKET_IO_NO_CONNECTION = 'no-connection'
const SOCKET_IO_CONNECTED = 'connected'
const SOCKET_IO_AUTHENTICATING = 'authenticating'
const SOCKET_IO_DISCONNECTED = 'disconnected'
const SOCKET_IO_RECONNECTING = 'reconnecting'

export {
  SOCKET_IO_SESSION_URL,
  SOCKET_IO_SETTINGS,
  SOCKET_IO_NO_CONNECTION,
  SOCKET_IO_CONNECTED,
  SOCKET_IO_AUTHENTICATING,
  SOCKET_IO_DISCONNECTED,
  SOCKET_IO_RECONNECTING
}
