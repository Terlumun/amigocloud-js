import { of, throwError, timer, Subject } from 'rxjs'
import {
  timeoutWith,
  switchMap,
  retryWhen,
  delayWhen,
  flatMap,
  first
} from 'rxjs/operators'

import BaseViewModel from './baseViewModel'
import BackgroundJobStatusModel from '../models/backgroundJobStatusModel'

const SOCKET_IO_TIMEOUT = 5000 // in ms
const RETRY_JOB_TIMEOUT = 1000 // in ms
const SUCCESS_STATUS = 'SUCCESS'
const FAILURE_STATUS = 'FAILURE'
const STOP_STATUS_CONDITIONS = [SUCCESS_STATUS, FAILURE_STATUS]

/**
 * View model that can be used when data is sent to the backend and we expect a
 * socket.io event being emited, it will fallback to background job polling when
 * an event hasn't been received in the desired amount of time.
 */
class PostSaveViewModel extends BaseViewModel {
  static Intent = {
    /**
     * Intent to wait for a list of events form the backend and in case
     * no event is emited, will poll on the jobs endpoint with the provided jobId
     * to try to generate a result so the js code keeps working
     * @name PostSaveViewModel.Intent.HandlePostSave
     * @constructor
     * @param {String} jobId - Job id we receive from the backend when we do an operation
     * @param {Array<String>} eventsMap - Events list to listen to
     * @param {Int?} timeout - Timeout in milliseconds, by default it is 5000
     */
    HandlePostSave: class {
      constructor (jobId, eventsList, timeout = SOCKET_IO_TIMEOUT) {
        this.jobId = jobId
        this.eventsList = eventsList
        this.timeout = timeout
      }
    },
    /**
     * Intent that receives and eventsMap instead of events list, this is
     * useful in case we want to map a determinate eventName to a SUCCESS state from
     * the background jobs endpoint, and another eventName to a FAILURE state
     * @name PostSaveViewModel.Intent.HandlePostSaveWithEventsMap
     * @constructor
     * @param {String} jobId - Job id we receive from the backend when we do an operation
     * @param {Object} eventsMap - Object mapping SUCCESS and FAILURE keys to event names
     * @param {Int?} timeout - Timeout in milliseconds, by default it is 5000
     */
    HandlePostSaveWithEventsMap: class {
      constructor (jobId, eventsMap, timeout = SOCKET_IO_TIMEOUT) {
        this.jobId = jobId
        this.eventsMap = eventsMap
        this.timeout = timeout
      }
    }
  }

  constructor (socketIOWrapperInstance) {
    super()

    this.socketIOWrapperInstance = socketIOWrapperInstance
    this.stopListenerSubject = new Subject()
  }

  stateTransformer (intentObservable) {
    return intentObservable.pipe(
      flatMap(intent => {
        switch (intent.constructor) {
          case PostSaveViewModel.Intent.HandlePostSave:
            return this.handlePostSave(
              intent.jobId,
              intent.eventsList,
              intent.timeout
            )
          case PostSaveViewModel.Intent.HandlePostSaveWithEventsMap:
            return this.handlePostSaveWithEventsMap(
              intent.jobId,
              intent.eventsMap,
              intent.timeout
            )
        }
      })
    )
  }

  pollData = (jobId, eventsList) => {
    const backgroundJobStatusModelInstance = new BackgroundJobStatusModel(jobId)

    return backgroundJobStatusModelInstance
      .getModelData({
        refresh: true
      })
      .pipe(
        switchMap(({ job }) => {
          this.stopListenerSubject.next(1)
          this.stopListenerSubject.complete()

          if (STOP_STATUS_CONDITIONS.indexOf(job.status) !== -1) {
            return of({
              fromJob: true,
              eventName: eventsList[0],
              eventData: job.extra,
              job: jobId
            })
          } else {
            return throwError(job)
          }
        }),
        retryWhen(error => {
          return error.pipe(delayWhen(() => timer(RETRY_JOB_TIMEOUT)))
        })
      )
  }

  handlePostSave = (jobId, eventsList, timeout) => {
    return this.socketIOWrapperInstance
      .listenOn(eventsList, this.stopListenerSubject)
      .pipe(first(), timeoutWith(timeout, this.pollData(jobId, eventsList)))
  }

  pollDataWithEventsMap = (jobId, eventsMap) => {
    const backgroundJobStatusModelInstance = new BackgroundJobStatusModel(jobId)

    return backgroundJobStatusModelInstance
      .getModelData({
        refresh: true
      })
      .pipe(
        switchMap(({ job }) => {
          this.stopListenerSubject.next(1)
          this.stopListenerSubject.complete()

          if (STOP_STATUS_CONDITIONS.indexOf(job.status) !== -1) {
            const responseObject = {
              fromJob: true,
              eventData: {
                ...job.extra,
                job: jobId
              }
            }
            if (job.status === SUCCESS_STATUS) {
              return of({
                ...responseObject,
                eventName: eventsMap.success
              })
            } else {
              return of({
                ...responseObject,
                eventName: eventsMap.failure
              })
            }
          } else {
            return throwError(job)
          }
        }),
        retryWhen(error => {
          return error.pipe(delayWhen(() => timer(RETRY_JOB_TIMEOUT)))
        })
      )
  }

  handlePostSaveWithEventsMap = (jobId, eventsMap, timeout) => {
    const eventsList = Object.values(eventsMap)

    return this.socketIOWrapperInstance
      .listenOn(eventsList, this.stopListenerSubject)
      .pipe(
        first(),
        timeoutWith(timeout, this.pollDataWithEventsMap(jobId, eventsMap))
      )
  }
}

export default PostSaveViewModel
