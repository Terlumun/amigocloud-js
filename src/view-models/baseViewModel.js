import { Subject, asyncScheduler, asapScheduler } from 'rxjs'
import { observeOn, startWith, shareReplay, subscribeOn } from 'rxjs/operators'

class BaseViewModel {
  constructor () {
    this.viewState = {}
    this.intentProcessor = new Subject()
    this.stateTransformer = this.stateTransformer.bind(this)
    this.initialState = this.initialState.bind(this)

    this.state = this.intentProcessor.pipe(
      observeOn(asyncScheduler),
      this.stateTransformer,
      startWith(this.initialState()),
      observeOn(asapScheduler),
      shareReplay(1),
      subscribeOn(asyncScheduler)
    )
  }

  intent (viewIntent) {
    this.intentProcessor.next(viewIntent)
  }

  initialState () {
    return this.viewState
  }

  stateTransformer (intentObservable) {
    console.warn('stateTransformer method should be overriden')
  }
}

export default BaseViewModel
