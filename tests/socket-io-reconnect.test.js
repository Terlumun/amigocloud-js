import axios from 'axios'
import nock from 'nock'
import BackendServer from './backend'

import settings from '../src/lib/settings'
import SocketIOWrapper from '../src/lib/SocketIOWrapper'

let backendServer
let client

axios.defaults.adapter = require('axios/lib/adapters/http')

describe('AmigoCloud socket.io-client reconnection', () => {
  beforeAll((done) => {
    backendServer = new BackendServer()

    // setting up test server settings
    const { address, port } = backendServer.getAddressInfo()
    settings.set({
      baseUrl: `http://[${address}]:${port}/`,
      userId: 1,
      accessToken: backendServer.validToken
    })

    done()
  })

  afterEach((done) => {
    backendServer.authenticated = false

    nock.restore()
    nock.cleanAll()

    done()
  })

  afterAll((done) => {
    if (client.socketIO && client.socketIO.connected) {
      client.socketIO.removeAllListeners()
      client.socketIO.disconnect()
    }

    backendServer.removeAllListeners()

    backendServer.close()
    done()
  })

  test('should retry the start_websocket_session endpoint when a server error happens', (done) => {
    client = new SocketIOWrapper()

    const { address, port } = backendServer.getAddressInfo()
    const scope = nock(`http://[${address}]:${port}`)
      .get('/api/v1/me/start_websocket_session')
      .query({ token: 'VALID_TOKEN'})
      .replyWithError({
        message: 'Connection Aborted Error',
        code: 'ECONNABORTED',
      })
      .get('/api/v1/me/start_websocket_session')
      .query({ token: 'VALID_TOKEN'})
      .reply(200, {
        websocket_session: 'SOCKET_IO_SESSION_TOKEN'
      })

    backendServer.on('connection', (mySocket) => {
      expect(mySocket).toBeDefined()
    })

    setTimeout(() => {
      expect(backendServer.authenticated).toBe(true)
      expect(client.socketIO.connected).toBe(true)
      // verify that it hit the endpoint twice because of the first request
      // was not a valid response
      expect(nock.isDone()).toBe(true)
      scope.done()
      done()
    }, 1000)
  })
})
